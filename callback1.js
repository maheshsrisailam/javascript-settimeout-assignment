const callback1 = (id, data, cb) => {
    setTimeout(() => {
        try{
            const result = data.find(element => element.id === id);
            const err = new Error("Data not found");
            result ? cb(null,result) : cb(err.message);
        } catch {
            console.log("Something went wrong")
        }
    }, 2 * 1000);
}

module.exports = callback1;