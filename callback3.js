const callback3 = (id, data, cb) => {
    setTimeout(() => {
        try {
            const result = Object.entries(data).find(element => element[0] === id);
            const err = new Error("Data Not Found");
            result ? cb(null, result) : cb(err.message);
        } catch {
            console.log("Something went wrong")
        }
    }, 2 * 1000);
}

module.exports = callback3;