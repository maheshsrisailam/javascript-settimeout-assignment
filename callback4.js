const callback4 = (callback1, callback2, callback3, boards, lists, cards) => {
    
    setTimeout(() => {
        try {
            let id = '';
            boards.map(element => {if (element.name === "Thanos") id = element.id})
            callback1(id, boards, (err, resultantBoard) => {
                if (resultantBoard){
                    //console.log(resultantBoard);
                    callback2(resultantBoard.id, lists, (err, resultantList) => {
                        if (resultantList){
                            //console.log(resultantList);
                            const mindId = resultantList[1].find(element => element.name === 'Mind');
                            callback3(mindId.id, cards, (err, resultantCard) => {
                                if (resultantCard){
                                    console.log(resultantCard);
                                }else{
                                    console.log(err);
                                }
                            });
                        }else{
                            console.log(err);
                        }
                    });
                }else{
                    console.log(err);
                }
            });
        } catch {
            console.log("Something went wrong")
        }
    }, 2 * 1000);
}

module.exports = callback4;