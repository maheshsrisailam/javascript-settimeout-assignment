const callback5 = (callback1, callback2, callback3, boards, lists, cards) => {
    setTimeout(() => {
        try {
            let id = '';
            boards.map(element => {if (element.name === "Thanos") id = element.id})
            callback1(id, boards, (err, resultandBoard) => {
                if (resultandBoard){
                    //console.log(resultandBoard);
                    callback2(resultandBoard.id, lists, (err, resultantList) => {
                        if (resultantList){
                            //console.log(resultantList);
                            resultantList[1].map(element => {
                                if(element.name === 'Mind'){
                                    callback3(element.id, cards, (err, resultantCard) => {
                                        if (resultantCard){
                                            console.log(resultantCard);
                                        }else{
                                            console.log(err);
                                        }
                                    });
                                }
                                else if (element.name === 'Space'){
                                    callback3(element.id, cards, (err, result) => {
                                        if (result){
                                            console.log(result);
                                        }else{
                                            console.log(err);
                                        }
                                    });
                                }
                            });   
                        }else{
                            console.log(err);
                        }
                    });
                }else{
                    console.log(err);
                }
            });
        } catch {
            console.log("Something went wrong")
        }
    },2*1000);
}

module.exports = callback5;