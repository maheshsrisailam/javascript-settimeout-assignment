const callback1 = require('../callback1');
const callback2 = require('../callback2');
const callback3 = require('../callback3');
const callback5 = require('../callback5');
const boards = require('./boards.js');
const lists = require('./lists.js');
const cards = require('./cards.js');

callback5(callback1, callback2, callback3, boards, lists, cards);